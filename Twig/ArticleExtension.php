<?php

namespace Bert\CmsApiBundle\Twig;

use Bert\CmsApiBundle\Service\ArticleService;
use Bert\ApiCms\Api;
use Bert\IspCoreBundle\Storage\UserStorage;
use Bert\IspCoreBundle\Service\EventBroker;

class ArticleExtension extends \Twig_Extension
{
    /**
     * @var ArticleService
     */
    private $service;

    /**
     * @param ArticleService $service
     */
    public function __construct(ArticleService $service)
    {
        $this->service = $service;
    }

    public function getFunctions()
    {
        return array(
             new \Twig_SimpleFunction('article', [$this, 'getArticle']),
        );
    }

    /**
     * @param string $tag
     * @param string $locale
     * @return array
     */
    public function getArticle($tag, $locale = null)
    {
        return $this->service->get($tag, $locale);
    }

    public function getName()
    {
        return 'article_extension';
    }
}