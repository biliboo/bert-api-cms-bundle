<?php

namespace Bert\CmsApiBundle\Controller;

use Bert\CmsApiBundle\Service\ArticleService;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class ArticleController extends Controller
{
    const PREFIX = ':';

    /**
     * @return Response
     */
    public function showAction(Request $request, $tag)
    {
        $locale = $request->query->get('locale', $request->query->get('l'));
        $prefix = $request->query->has('prefix')
            ? $request->query->get('prefix') . self::PREFIX
            : ''
        ;

        $article = $this->getArticleService()->get($prefix . urldecode($tag), $locale);

        if (false === $article) {
            return new JsonResponse([
                'content' => $this->renderView('BertCmsApiBundle::modal_article_fallback.html.twig'),
            ]);
        }

        return new JsonResponse([
            'content' => $this->renderView('BertCmsApiBundle::modal_article.html.twig', [
                'article' => $article,
            ]),
        ]);
    }

    /**
     * @return ArticleService
     */
    private function getArticleService()
    {
        return $this->get(ArticleService::class);
    }
}
