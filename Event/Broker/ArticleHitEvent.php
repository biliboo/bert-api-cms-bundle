<?php

namespace Bert\CmsApiBundle\Event\Broker;

use Bert\IspCoreBundle\Event\Broker\BrokerEvent;

class ArticleHitEvent extends BrokerEvent
{
    const EVENT = 'broker.cms.article_hit';

    public function __construct($id, $tag, $locale, $uuid)
    {
        parent::__construct([
            'id'     => $id,
            'tag'    => $tag,
            'locale' => $locale,
            'uuid'   => $uuid,
            'date'   => time(),
        ]);
    }
}