<?php

namespace Bert\CmsApiBundle\Event\Broker;

use Bert\IspCoreBundle\Event\Broker\BrokerEvent;

class ArticleUpdatedEvent extends BrokerEvent
{
    const EVENT = 'broker.cms.article_updated';
}