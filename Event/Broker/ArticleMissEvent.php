<?php

namespace Bert\CmsApiBundle\Event\Broker;

use Bert\IspCoreBundle\Event\Broker\BrokerEvent;

class ArticleMissEvent extends BrokerEvent
{
    const EVENT = 'broker.cms.article_miss';

    public function __construct($tag, $locale, $uuid)
    {
        parent::__construct([
            'tag'    => $tag,
            'locale' => $locale,
            'uuid'   => $uuid,
            'date'   => time(),
        ]);
    }
}