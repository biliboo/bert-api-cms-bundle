<?php

namespace Bert\CmsApiBundle\EventListener;

use Bert\ApiCms\Api;
use Bert\CmsApiBundle\Event\Broker\ArticleUpdatedEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class BrokerEventListener implements EventSubscriberInterface
{
    /**
     * @var Api
     */
    private $api;

    /**
     * @param Api $api
     */
    public function __construct(Api $api)
    {
        $this->api = $api;
    }

    public static function getSubscribedEvents()
    {
        return [
            ArticleUpdatedEvent::EVENT => 'onArticleUpdated',
        ];
    }

    /**
     * @param ArticleUpdatedEvent $event
     */
    public function onArticleUpdated(ArticleUpdatedEvent $event)
    {
        foreach($event->getData('tags', []) as $tag) {
            $this->api->invalidateArticle($tag, $event->getData('locale', 'fr'));
        }
    }
}
