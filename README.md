# Usage

## AppKernel.php
```php
class AppKernel extends Kernel
{
    public function registerBundles()
    {
        $bundles = [
            new Bert\ApiCoreBundle\BertApiCoreBundle(),
            new Bert\CmsApiBundle\BertCmsApiBundle(),
        ];
    }
}
```

## routing.yml

```yaml
cms:
    resource: '@BertCmsApiBundle/Resources/config/routing.yml'
    host:     "%host_front%"
    prefix:   /cms
```

## config.yml

```yaml
bert_cms_api:
    options:
        endpoint:      "https://%host_cms_api%"
        client_id:     "%client_id%"
        client_secret: "%client_secret%"
        redirect_uri:  "%client_redirect_uri%"
```
