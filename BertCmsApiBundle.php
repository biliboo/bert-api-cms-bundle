<?php

namespace Bert\CmsApiBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class BertCmsApiBundle extends Bundle
{
    const APP_CODE = 'cms';
}
