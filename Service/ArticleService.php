<?php

namespace Bert\CmsApiBundle\Service;

use Bert\ApiCms\Api;
use Bert\CmsApiBundle\BertCmsApiBundle;
use Bert\CmsApiBundle\Event\Broker\ArticleMissEvent;
use Bert\CmsApiBundle\Event\Broker\ArticleHitEvent;
use Bert\IspCoreBundle\Storage\UserStorage;
use Bert\IspCoreBundle\Service\EventBroker;

class ArticleService
{
    /**
     * @var UserStorage
     */
    private $storage;

    /**
     * @var EventBroker
     */
    private $broker;

    /**
     * @var Api
     */
    private $api;

    /**
     * @param UserStorage $storage
     * @param Api $api
     */
    public function __construct(
        UserStorage $storage,
        EventBroker $broker,
        Api $api)
    {
        $this->storage = $storage;
        $this->broker  = $broker;
        $this->api     = $api;
    }

    /**
     * @param $tag
     * @param null $locale
     * @param User|null $user
     * @return array
     */
    public function get($tag, $locale = null, User $user = null)
    {
        if (null === $user) {
            $user = $this->storage->getUser();
        }

        $uuid = null;

        if (null !== $user)
        {
            if (null === $locale) {
                $locale = $user->getLocale();
            }

            $uuid = $user->getUuid();
        }

        $article = $this->api->getArticle($tag, $locale);

        if (false !== $article) {
            $this->broker->publish(new ArticleHitEvent($article['id'], $tag, $locale, $uuid), BertCmsApiBundle::APP_CODE);
        } else {
            $this->broker->publish(new ArticleMissEvent($tag, $locale, $uuid), BertCmsApiBundle::APP_CODE);
        }

        return $article;
    }
}